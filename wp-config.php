<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'vtj-cambodia');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Er5(76b=?t*~iM#Sdjti 4%2lPkk$Yt&wn?Yx+$uLAQ!/nE6?V==]>|,N y/UJvu');
define('SECURE_AUTH_KEY',  '<9XVrJF}]YH>a&mEf8D~jJQ}RL0RYq8VF`cSETsFW*|D=4kFiE~DoN}d$sgeYh,q');
define('LOGGED_IN_KEY',    '*I8Aa?vUHeY,Q<L@wAi4%@#|CK(m4#hE{yR9 ;jsFq,Q<+!s?BBvQ-jyal75%Ty|');
define('NONCE_KEY',        '+q> #U=3H9QRHV%.0FDR9-OUuAXbqlR(FSX|r=1.8w)FxE,yh${4)_dX|Nt]-Vm8');
define('AUTH_SALT',        'kjDs9swAm&&v~k-p;>$+{2n((`r%wCb+QSlGkyX%f8%AZ70%$70(yH*qU#}uf.DB');
define('SECURE_AUTH_SALT', 'Ma.I-Ts/e6%q*[*Sa;3vv^ vxY7c e}/Hy7fA,^o+Pw7,iE^>#QE}`$I6{T_cO^V');
define('LOGGED_IN_SALT',   'IhE-eOF<IY`;J(+|-Q}sE&4_co3@y_Y9)##ibt?N3o(LXo5U6HhQkL=@w-9vlKuk');
define('NONCE_SALT',       ']9KxuwSL]?;-8@9=mM;gbY=8wo=U5&{52=lw2ka7@qkAq)39d_ SfJkbjM;SLG}H');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
